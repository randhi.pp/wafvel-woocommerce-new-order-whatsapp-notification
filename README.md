# Wafvel WooCommerce New Order Whatsapp Notification

a Wordpress plugin to enable sending new order Notification to admin and customer using whatsapp via Wafvel.com

Features :
- BACS / bank info in chat received by both admin / customer.
- Midtrans supported.
- Link to pay in chat received.

<img src="img/admin.jpg"  width="300"> &nbsp;&nbsp; <img src="img/cust.jpg"  width="300">
<img src="img/wafvel-options-page.png"  width="1368">

*** Note : This plugin require WooCommerce plugin installed on your Wordpress. ***

### Step 1 : Download plugin

https://gitlab.com/randhi.pp/wafvel-woocommerce-new-order-whatsapp-notification/-/archive/master/wafvel-woocommerce-new-order-whatsapp-notification-master.zip

### Step 2 : Install plugin


1. Go to you wordpress admin dashboard -> plugin -> add new    
https://domain.com/wp-admin/plugin-install.php?tab=upload

2. upload downloaded file from step 1

3. Go to admin dashboard -> options -> wafvel options    
https://domain.com/wp-admin/options-general.php?page=wafvel-options-page


### Step 3 : Input Wafvel Token

#### How to get Token

    1. Register at wafvel.com/register - it's Free!
    2. Add your whatsapp number on add whatsapp page
    3. Copy your token
    4. Scan QR (like you use whatsapp web) using your phone whatsapp here
    5. Paste to wafvel options page on wordpress

### Step 4 : Input Admin Phone

This admin phone means any phone number (with whatsapp active)
which you want to send admin notification message to.
